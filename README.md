# Battleship
University project that uses a client-server and peer-to-peer architecture to play the game.
## Game
![Game](./docs/images/game.png)
![Empty field](./docs/images/empty-field.png)
![Ship preview](./docs/images/ship-preview.png)
## Client server
![Client server architecture](./docs/images/client-server-architecture.png)
## Peer to peer
![Peer to peer architecture](./docs/images/peet-to-peer-architecture.png)
