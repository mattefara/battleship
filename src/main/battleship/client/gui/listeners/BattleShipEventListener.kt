package client.gui.listeners

import client.game.Ship

interface BattleShipEventListener {
    fun onGameStarted()
    fun onShipsPlaced(ship: Ship?)
    fun onGameEnded(uuid: String)
}