package client.gui.listeners

import client.game.AttackPayload

interface PlayerListener {

    fun onReceivedMessageFromOpponent(payload: AttackPayload): AttackPayload
    fun onReceivedMessageFromPlayer(payload: AttackPayload)
    fun onReceivedMessage(payload: AttackPayload)
    fun onError()

}