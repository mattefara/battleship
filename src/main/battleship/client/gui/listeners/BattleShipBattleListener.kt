package client.gui.listeners

import client.game.Position


interface BattleShipBattleListener {
    fun onMissileFired(position: Position)
    fun onInvalidPosition(position: Position)
}