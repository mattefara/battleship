package client.gui.canvas

import client.game.Position
import java.awt.Color
import java.awt.Graphics
import java.awt.event.MouseEvent
import java.awt.event.MouseMotionListener

open class HoveringCanvas: BaseCanvas(), MouseMotionListener {

    private var currentPosition: Position? = null

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        if (enabled && !mouseMotionListeners.any { listener -> listener == this }) {
            addMouseMotionListener(this)
        } else {
            removeMouseMotionListener(this)
        }
    }

    open fun togglePreview(graphic2d: Graphics, position: Position, color: Color) {
        saveAndRestoreGraphicColor(graphic2d, color) {
            drawCell(graphic2d, Position(position.x + 1, position.y + 1))
        }
    }

    override fun drawBoard(graphic2d: Graphics) {
        currentPosition?.let { togglePreview(graphic2d, it, Color.BLUE) }
        super.drawBoard(graphic2d)
    }

    override fun mouseDragged(p0: MouseEvent?) {

    }

    override fun mouseMoved(p0: MouseEvent?) {
        val position = getCellFromPosition(p0!!.x, p0.y) ?: return
        if (position == currentPosition) {
            return
        }

        currentPosition = position
        repaint()
    }

}