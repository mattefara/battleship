package client.gui.canvas

import client.game.Board
import client.game.Position
import java.awt.*
import javax.swing.JPanel

open class BaseCanvas(
    internal val configuration: CanvasConfiguration = CanvasConfiguration()
): JPanel() {

    internal val board = Board()

    init {
        preferredSize = Dimension(configuration.width, configuration.height)
        minimumSize = preferredSize
    }

    internal fun drawCell(
        graphic2d: Graphics,
        position: Position,
        filled: Boolean = true,
        offsetX: Int = configuration.offset,
        offsetY: Int = configuration.offset
    ) {
        val x = position.x * configuration.cellSize + offsetX
        val y = position.y * configuration.cellSize + offsetY
        if (filled) {
            graphic2d.fillRect(x, y, configuration.cellSize, configuration.cellSize)
        } else {
            graphic2d.drawRect(x, y, configuration.cellSize, configuration.cellSize)
        }
    }

    internal fun getCellFromPosition(x: Int, y: Int): Position? {
        val adjustedX = x - configuration.cellSize
        val adjustedY = y - configuration.cellSize

        if (adjustedX - configuration.offset < 0 || adjustedY - configuration.offset < 0) {
            return null
        }

        val position = coordsToPos(adjustedX, adjustedY)

        if (position.x in 0 until configuration.cols && position.y in 0 until configuration.rows) {
            return position
        }
        return null

    }

    private fun coordsToPos(x: Int, y: Int): Position {
        return Position(
            ((x - configuration.offset) / configuration.cellSize),
            ((y - configuration.offset) / configuration.cellSize)
        )
    }

    internal fun saveAndRestoreGraphicColor(graphic2d: Graphics, new: Color, block: () -> Unit) {
        val color = graphic2d.color
        graphic2d.color = new
        block()
        graphic2d.color = color
    }

    private fun drawText(graphic2d: Graphics, text: String, transformPosition: (width: Int, height: Int) -> Position) {
        val width = graphic2d.fontMetrics.stringWidth(text)
        val height = graphic2d.fontMetrics.height
        val position = transformPosition(width,height)
        graphic2d.drawString(text, position.x, position.y)
    }

    fun drawLetters(graphic2d: Graphics) {
        for (col in 0 until  configuration.cols) {
            val text = ('A' + col).toString()
            drawText(graphic2d, text) { width, height ->
                Position(
                    col * configuration.cellSize + configuration.offset - width / 2 + configuration.cellSize / 2 + configuration.cellSize,
                    configuration.cellSize + configuration.offset - height / 2
                )
            }
        }
    }

    fun drawNumbers(graphic2d: Graphics) {
        for (row in 1 .. configuration.rows) {
            val text = "$row"
            drawText(graphic2d, text) { width, height ->
                Position(
                    configuration.cellSize + configuration.offset - width / 2 - configuration.cellSize / 2,
                    row * configuration.cellSize + configuration.offset - height / 2 + configuration.cellSize
                )
            }
        }
    }

    open fun drawBoard(graphic2d: Graphics) {

        val startX = configuration.cellSize + configuration.offset
        val startY = configuration.cellSize + configuration.offset

        for (row in 0 until configuration.rows) {
            for (col in 0 until configuration.cols) {
                val position = Position(col, row)
                val info = when (board.getCell(Position(col, row))) {
                    Board.CellStatus.PLAYER_HIT -> Pair(Color.RED, true)
                    Board.CellStatus.PLAYER_MISS -> Pair(Color.YELLOW, true)
                    Board.CellStatus.PLAYER_SHIP -> Pair(Color.GRAY, true)
                    else -> Pair(Color.BLACK, false)
                }
                saveAndRestoreGraphicColor(graphic2d, info.first) {
                    drawCell(graphic2d, position, info.second, startX, startY)
                    saveAndRestoreGraphicColor(graphic2d, Color.BLACK) {
                        if (info.second) {
                            drawCell(graphic2d, position, false, startX, startY)
                        }
                    }
                }
            }
        }
    }

    override fun paintComponent(g: Graphics?) {
        super.paintComponent(g)
        val graphic2d = g as Graphics2D

        graphic2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

        drawLetters(graphic2d)
        drawNumbers(graphic2d)
        drawBoard(graphic2d)

    }
}