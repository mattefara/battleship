package client.gui.canvas


data class CanvasConfiguration(
    val offset: Int = 10,
    val cellSize: Int = 30,
    val rows: Int = 10,
    val cols: Int = 10,
    val width: Int = 2 * offset + (cols + 1) * cellSize,
    val height: Int = width
)

