package client.gui.canvas

import client.game.Board
import client.game.Position
import client.gui.listeners.BattleShipBattleListener
import java.awt.event.MouseEvent
import java.awt.event.MouseListener


class OpponentBattleShipCanvas(
    private var startPlayer: Boolean
): HoveringCanvas(), MouseListener {

    var battleShipBattleListener: BattleShipBattleListener? = null
        set(value) {
            field = value
        }

    init {
        isEnabled = false
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        if (enabled && !mouseListeners.any { listener -> listener == this }) {
            addMouseListener(this)
        } else {
            removeMouseListener(this)
        }
    }

    fun changeTurn() {
        startPlayer = !startPlayer
    }

    fun changeCellStatus(position: Position, status: Board.CellStatus) {
        board.changeCellStatus(position, status)
        repaint()
    }

    override fun mouseClicked(p0: MouseEvent?) {
        if (!startPlayer) {
            return
        }
        val position = getCellFromPosition(p0!!.x, p0.y) ?: return


        when (board.getCell(position)) {
            Board.CellStatus.PLAYER_WATER -> {
                battleShipBattleListener?.onMissileFired(position)
            }

            else -> {
                battleShipBattleListener?.onInvalidPosition(position)
            }
        }

    }

    override fun mousePressed(p0: MouseEvent?) {

    }

    override fun mouseReleased(p0: MouseEvent?) {

    }

    override fun mouseEntered(p0: MouseEvent?) {

    }

    override fun mouseExited(p0: MouseEvent?) {

    }
}