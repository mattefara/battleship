package client.gui.canvas

import client.game.Position
import client.game.Ship
import client.game.ShipDispatcher
import client.gui.listeners.BattleShipEventListener
import java.awt.Color
import java.awt.Graphics
import java.awt.event.MouseEvent
import java.awt.event.MouseListener

class BattleshipCanvas: HoveringCanvas(), MouseListener {

    private val shipDispatcher = ShipDispatcher()

    private var battleShipEventListener : BattleShipEventListener? = null

    private var currentOrientation = Ship.Orientation.HORIZONTAL

    init {
        isEnabled = true
    }

    fun setBattleShipEventListener(battleShipEventListener: BattleShipEventListener) {
        this.battleShipEventListener = battleShipEventListener
    }

    fun hasPlacedAllShips(): Boolean {
        return !shipDispatcher.getController().hasShip()
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        if (enabled && !mouseListeners.any { listener -> listener == this }) {
            addMouseListener(this)
        } else {
            removeMouseListener(this)
        }
    }

    override fun togglePreview(graphic2d: Graphics, position: Position, color: Color) {
        val controller = shipDispatcher.getController()
        saveAndRestoreGraphicColor(graphic2d, Color.BLUE) {
            controller.getShip(position, currentOrientation).getPositions().forEach { position ->
                if (position.x + 1 > configuration.cols || position.y + 1 > configuration.rows) {
                    return@forEach
                }
                drawCell(graphic2d, Position(position.x + 1, position.y + 1))
            }
        }
    }

    fun swapShipOrientation() {
        if (!isEnabled) {
            return
        }
        currentOrientation = if (currentOrientation == Ship.Orientation.HORIZONTAL) {
            Ship.Orientation.VERTICAL
        } else {
            Ship.Orientation.HORIZONTAL
        }
        repaint()
    }

    override fun mouseMoved(p0: MouseEvent?) {
        if (hasPlacedAllShips()) {
            return
        }
        super.mouseMoved(p0)
    }

    override fun mouseClicked(p0: MouseEvent?) {
        if (hasPlacedAllShips()) {
            return
        }

        val position = getCellFromPosition(p0!!.x, p0.y) ?: return

        if (!board.isValidPosition(position)) {
            return
        }

        val controller = shipDispatcher.getController()
        val ship = controller.placeShip(board, position, currentOrientation)
        battleShipEventListener?.onShipsPlaced(ship)
        repaint()
    }

    override fun mousePressed(p0: MouseEvent?) {}

    override fun mouseReleased(p0: MouseEvent?) {}

    override fun mouseEntered(p0: MouseEvent?) {}

    override fun mouseExited(p0: MouseEvent?) {}

    fun checkForHit(position: Position): Ship? {
        return shipDispatcher.getShip(position)
    }

    fun updateBoard(position: Position) {
        board.fire(position)
        repaint()
    }

    fun hasShipSank(position: Position): Boolean {
        val ship = shipDispatcher.getShip(position) ?: return false
        return ship.hasSank()
    }

    fun getDispatcher() = shipDispatcher
}