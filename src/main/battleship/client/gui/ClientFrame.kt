package client.gui

import client.Peer
import client.game.*
import client.gui.canvas.BattleshipCanvas
import client.gui.canvas.OpponentBattleShipCanvas
import client.gui.listeners.BattleShipBattleListener
import client.gui.listeners.BattleShipEventListener
import client.gui.listeners.PlayerListener
import client.gui.workers.InfoQueuer
import client.gui.workers.PlayerWaiter
import java.awt.Dimension
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import java.util.*
import javax.swing.BoxLayout
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JPanel


class ClientFrame(
    connectionHolder: Peer.ConnectionHolder,
) : JFrame("Battleship Client"), KeyListener, BattleShipEventListener, BattleShipBattleListener, PlayerListener {

    private val playerUUID = UUID.randomUUID().toString()

    private var startPlayer = connectionHolder.payload.start
    private var manager = GameManager(connectionHolder, this, playerUUID)

    private val waiter = PlayerWaiter(this)
    private val queuer = InfoQueuer(this)

    private val playerCanvas = BattleshipCanvas().apply {
        setBattleShipEventListener(this@ClientFrame)
    }

    private val opponentCanvas = OpponentBattleShipCanvas(startPlayer).apply {
        battleShipBattleListener = this@ClientFrame
    }

    private val infoLabel = JLabel("Start placing your ships. Press SPACE to change the orientation").apply {
        preferredSize = Dimension(400, preferredSize.height)
    }

    init {
        defaultCloseOperation = EXIT_ON_CLOSE

        val boardContainer = JPanel()
        boardContainer.add(playerCanvas)
        boardContainer.add(opponentCanvas)

        val infoContainer = JPanel().apply {
            add(infoLabel)
            minimumSize = preferredSize
            maximumSize = preferredSize
        }

        contentPane.layout = BoxLayout(contentPane, BoxLayout.PAGE_AXIS)
        contentPane.add(boardContainer)
        contentPane.add(infoContainer)

        pack()

        addKeyListener(this)

        isVisible = true

        waiter.execute()
        queuer.execute()
    }

    private fun changeTurn() {
        startPlayer = !startPlayer
        opponentCanvas.changeTurn()
        val text = if (startPlayer) {
            "It's your turn... Fire!"
        } else {
            "Waiting for the opponent to make a move"
        }
        updateStatus(text)
    }

    override fun keyTyped(p0: KeyEvent?) {
        when (p0?.keyChar) {
            ' ' -> playerCanvas.swapShipOrientation()
            else -> return
        }
    }

    override fun keyPressed(p0: KeyEvent?) {

    }

    override fun keyReleased(p0: KeyEvent?) {

    }

    override fun onGameStarted() {
        manager.startGame()
        opponentCanvas.isEnabled = true
        val text = if (startPlayer) {
            "It's your turn... Fire!"
        } else {
            "Waiting for the opponent to make a move"
        }
        updateStatus(text)
    }

    override fun onShipsPlaced(ship: Ship?) {
        if (ship == null) {
            updateStatus("Cannot place ship in the given position!")
        } else {
            updateStatus("Placed ship!")
        }

        if (playerCanvas.hasPlacedAllShips()) {
            playerCanvas.isEnabled = false
            manager.playerReady()
            updateStatus("All ships placed... Waiting for the opponent")
        }
    }

    override fun onGameEnded(uuid: String) {
        updateStatus("Ending the game")

        synchronized(this) {
            updateStatus(
                if (manager.checkPlayer(uuid)) {
                    "You have won the game! Congrats!"
                } else {
                    "You have lost the game, all your ships have sunk!!"
                }
            )
        }
        playerCanvas.isEnabled = false
        opponentCanvas.isEnabled = false
    }

    override fun onMissileFired(position: Position) {
        manager.sendAttackPayload(position)
    }

    override fun onInvalidPosition(position: Position) {
        updateStatus("Cannot fire from this position, invalid cell!")
    }

    override fun onReceivedMessageFromOpponent(payload: AttackPayload): AttackPayload {
        if (payload.ready == true) {
            manager.opponentReady()
            return payload
        }

        val ship = playerCanvas.checkForHit(payload.position)
        ship?.apply {
            takeHit()
        }
        playerCanvas.updateBoard(payload.position)
        val sunk = playerCanvas.hasShipSank(payload.position)

        val winner = if (sunk && playerCanvas.getDispatcher().hasAllShipSunk()) {
            payload.sender
        } else {
            null
        }

        val message = AttackPayload(payload.sender, payload.position, ship != null, sunk, winner)
        manager.sendAttackPayload(message)
        return message
    }

    override fun onReceivedMessageFromPlayer(payload: AttackPayload) {
        val status = if (payload.hit == true) Board.CellStatus.PLAYER_HIT else Board.CellStatus.PLAYER_MISS
        if (payload.hit == true) {
            updateStatus("You have hit a ship, keep going!")
        }

        if (payload.sinkShip == true) {
            updateStatus("You have sunk a ship, keep going!")
        }

        synchronized(this@ClientFrame) {
            opponentCanvas.changeCellStatus(payload.position, status)
        }
    }

    override fun onReceivedMessage(payload: AttackPayload) {
        if (payload.winner != null) {
            return onGameEnded(payload.winner)
        }

        if (manager.isGameStarted() && payload.hit != true) {
            changeTurn()
        }
    }

    override fun onError() {
        onGameEnded(playerUUID)
        updateStatus("The opponent disconnected! You win!")
    }

    fun canStart(): Boolean {
        return manager.isReady()
    }

    fun updateStatus(status: String) {
        queuer.addMessage(status)
    }

    fun writeStatus(status: String) {
        infoLabel.text = status
    }
}