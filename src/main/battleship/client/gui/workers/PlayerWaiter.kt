package client.gui.workers

import client.gui.ClientFrame
import javax.swing.SwingWorker

class PlayerWaiter(
    private val frame: ClientFrame
) : SwingWorker<Boolean, Boolean>() {
    override fun doInBackground(): Boolean {
        while (!frame.canStart()) {
            Thread.sleep(200)
        }
        return true
    }

    override fun done() {
        println("Worker is done")
        frame.updateStatus("Starting the game")
        frame.onGameStarted()
    }
}