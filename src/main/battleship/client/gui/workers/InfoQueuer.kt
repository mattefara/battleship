package client.gui.workers

import client.gui.ClientFrame
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue
import javax.swing.SwingWorker

class InfoQueuer(
    private val clientFrame: ClientFrame
) : SwingWorker<String, String>() {

    private val queue: BlockingQueue<String> = LinkedBlockingQueue()

    fun addMessage(message: String) {
        queue.put(message)
    }

    override fun doInBackground(): String {
        while (true) {
            val message = queue.take()
            publish(message)
            Thread.sleep(700)
        }
    }

    override fun process(chunks: MutableList<String>?) {
        chunks?.first()?.let { clientFrame.writeStatus(it) }
    }

}