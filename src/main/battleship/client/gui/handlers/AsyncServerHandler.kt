package client.gui.handlers

import client.game.AttackPayload
import client.gui.listeners.PlayerListener
import core.handlers.AsyncHandler
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.net.Socket
import java.util.*

class AsyncServerHandler(
    socket: Socket,
    val playerId: String = UUID.randomUUID().toString()
) : AsyncHandler(socket) {

    var playerListener: PlayerListener? = null

    override fun onMessageReceived(message: String) {
        val payload = Json.decodeFromString<AttackPayload>(message)

        val newMessage = if (payload.sender == playerId) {
            playerListener?.onReceivedMessageFromPlayer(payload)
            payload
        } else {
            playerListener?.onReceivedMessageFromOpponent(payload)
        }

        playerListener?.onReceivedMessage(newMessage!!)

    }

    override fun onError(ex: Throwable) {
        super.onError(ex)
        playerListener?.onError()
    }
}