package client

import core.data.ConnectionPayload
import core.handlers.Handler
import core.server.AsyncServer
import java.net.InetAddress
import java.net.Socket

class Peer(
    private val serverAddress: String,
    private val serverPort: Int,
) {

    class ConnectionHolder(
        val serverConnection: Socket,
        val clientConnection: Socket,
        val payload: ConnectionPayload
    )

    private var server = AsyncServer(0)
    private val serverThread = Thread(server).apply {
        start()
    }

    private var remoteClient = runCatching { Handler(Socket(serverAddress, serverPort)) }
        .onFailure { server.close() }
        .getOrThrow()

    fun establishConnection(): ConnectionHolder {
        remoteClient.writeMessage(
            ConnectionPayload(
                InetAddress.getLocalHost().hostAddress,
                server.serverSocket.localPort
            )
        )

        val data = remoteClient.readMessage<ConnectionPayload>()
        val peer = Handler(Socket(data.address, data.port))

        serverThread.join()

        return ConnectionHolder(server.connection, peer.socket, data)
    }

}