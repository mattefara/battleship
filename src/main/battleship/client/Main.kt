package client

import client.gui.ClientFrame
import core.Config
import core.Logger
import javax.swing.SwingUtilities
import kotlin.system.exitProcess

fun main() {

    val serverAddress = System.getenv("SERVER_ADDRESS") ?: Config.DEFAULT_HOST

    val peer = runCatching {
        Peer(serverAddress, 6789)
    }.onFailure {
        Logger.log(Peer::class.java, "Cannot connect to the server")
        exitProcess(1)
    }

    var connectionHolder = runCatching {
        peer.getOrNull()!!.establishConnection()
    }.onFailure {
        Logger.log(Peer::class.java, "Error while establishing peer to peer connection")
        Logger.log(Peer::class.java, "Attempting new connection")
    }

    if (connectionHolder.getOrNull() == null) {
        connectionHolder = Result.success(Peer(serverAddress, 6789).establishConnection())
    }


    SwingUtilities.invokeLater {
        ClientFrame(
            connectionHolder.getOrNull()!!
        )
    }
}