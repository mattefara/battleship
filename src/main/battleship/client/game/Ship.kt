package client.game

fun Boolean.toInt() = if (this) 1 else 0

class Ship(private val origin: Position, private val length: Int, private val orientation: Orientation = Orientation.HORIZONTAL) {

    private var takenHits = 0

    enum class Orientation {
        HORIZONTAL,
        VERTICAL
    }

    fun getPositions(): Array<Position> {
        arrayListOf<Position>()
        val isHorizontal = orientation == Orientation.HORIZONTAL
        return Array(length) {
            Position(origin.x + (it * isHorizontal.toInt()), origin.y + (it * (!isHorizontal).toInt()))
        }
    }

    fun takeHit() {
        takenHits++
    }

    fun hasSank(): Boolean {
        return takenHits >= length
    }

}