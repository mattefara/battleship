package client.game

class ShipController(private val shipLength: Int, private val shipsNumber: Int) {

    private var shipCounter = shipsNumber
    private val placedShips = arrayListOf<Ship>()

    fun hasShip(): Boolean {
        return shipCounter > 0
    }

    fun placeShip(board: Board, position: Position, orientation: Ship.Orientation): Ship? {
        if (!hasShip()) {
            return null
        }
        val ship = Ship(position, shipLength, orientation)
        val result = board.placeShip(ship)
        if (!result) {
            return null
        }
        --shipCounter

        placedShips.add(ship)
        return ship
    }

    override fun toString(): String {
        return "Ship of length $shipLength [$shipCounter/$shipsNumber]"
    }

    fun getShip(position: Position, orientation: Ship.Orientation): Ship {
        return Ship(position, shipLength, orientation)
    }

    fun getShip(position: Position): Ship? {
        for (ship in placedShips) {
            if (position in ship.getPositions()) {
                return ship
            }
        }
        return null
    }

    fun hasAllSunk(): Boolean {
        return placedShips.all { ship -> ship.hasSank() }
    }
}