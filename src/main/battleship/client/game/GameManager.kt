package client.game

import client.Peer
import client.gui.ClientFrame
import client.gui.handlers.AsyncServerHandler
import core.handlers.Handler

class GameManager(
    connectionHolder: Peer.ConnectionHolder,
    private val clientFrame: ClientFrame,
    private val uuid: String
) {

    private val clientHandler = Handler(connectionHolder.clientConnection)
    private val serverHandler = AsyncServerHandler(connectionHolder.serverConnection, uuid)

    private var opponentReady = false
    private var playerReady = false
    private var gameStared = false

    init {
        Thread(serverHandler.apply {
            playerListener = clientFrame
        }).start()
    }

    fun playerReady() {
        playerReady = true
        sendAttackPayload(AttackPayload(serverHandler.playerId, Position(-1, -1), ready = true))
    }

    fun checkPlayer(uuid: String): Boolean {
        return this.uuid == uuid
    }

    fun opponentReady() {
        opponentReady = true
    }

    fun startGame() {
        gameStared = true
    }

    fun sendAttackPayload(payload: AttackPayload) {
        clientHandler.writeMessage(payload)
    }

    fun sendAttackPayload(position: Position) {
        clientHandler.writeMessage(AttackPayload(serverHandler.playerId, position))
    }

    fun isReady(): Boolean {
        return playerReady && opponentReady
    }

    fun isGameStarted(): Boolean {
        return gameStared && isReady()
    }

}