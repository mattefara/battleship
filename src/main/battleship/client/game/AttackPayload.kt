package client.game

import kotlinx.serialization.Serializable

@Serializable
data class AttackPayload(
    val sender: String,
    val position: Position,
    val hit: Boolean? = null,
    val sinkShip: Boolean? = null,
    val winner: String? = null,
    val ready: Boolean? = null
)
