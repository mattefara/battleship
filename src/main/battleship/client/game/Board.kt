package client.game

class Board {

    companion object {
        private const val ROWS = 10
        private const val COLS = 10
    }

    private val cells = Array(ROWS) {
        Array(COLS) {
            CellStatus.PLAYER_WATER
        }
    }

    enum class CellStatus {
        PLAYER_WATER,
        PLAYER_SHIP,
        PLAYER_HIT,
        PLAYER_MISS,

        // Status when cell status is already HIT or MISS and the player tries to fire two times in the same spot
        // or is outside the board
        INVALID
    }

    fun isValidPosition(position: Position): Boolean {
        return (position.x in 0 until COLS) && (position.y in 0 until ROWS)
    }

    fun isValidCell(status: CellStatus): Boolean {
        return status == CellStatus.PLAYER_WATER
    }

    private fun isValidShipPosition(ship: Ship): Boolean {
        return ship.getPositions().all { position ->
            isValidPosition(position) && isValidCell(getCell(position))
        }
    }

    fun getCell(position: Position): CellStatus {
        return cells[position.x][position.y]
    }

    fun changeCellStatus(position: Position, status: CellStatus) {
        cells[position.x][position.y] = status
    }

    fun placeShip(ship: Ship): Boolean {
        if (!isValidShipPosition(ship)) {
            return false
        }
        ship.getPositions().forEach {position ->
            changeCellStatus(position, CellStatus.PLAYER_SHIP)
        }
        return true
    }

    fun fire(position: Position): CellStatus {
        if (!isValidPosition(position)) {
            return CellStatus.INVALID
        }
        val nextStatus = when (getCell(position)) {
            CellStatus.PLAYER_WATER -> CellStatus.PLAYER_MISS
            CellStatus.PLAYER_SHIP -> CellStatus.PLAYER_HIT
            else -> CellStatus.INVALID
        }
        if (nextStatus != CellStatus.INVALID) {
            changeCellStatus(position, nextStatus)
        }
        return nextStatus
    }

}