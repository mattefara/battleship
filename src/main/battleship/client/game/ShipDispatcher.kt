package client.game

class ShipDispatcher {

    private val ships = arrayOf(
        ShipController(5, 1),
        ShipController(4, 1),
        ShipController(3, 1),
        ShipController(2, 2),
        ShipController(1, 2),
    )
    private var currentShipIndex = 0

    fun getController(): ShipController {
        for (i in currentShipIndex until ships.size) {
            val controller = ships[i]
            if (controller.hasShip()) {
                currentShipIndex = i
                return controller
            }
        }
        currentShipIndex = ships.size
        return ships.last()
    }

    fun getShip(position: Position): Ship? {
        for (controller in ships) {
            val ship = controller.getShip(position)
            if (ship != null) {
                return ship
            }
        }
        return null
    }

    fun hasAllShipSunk(): Boolean {
        return ships.all { shipController -> shipController.hasAllSunk() }
    }

}