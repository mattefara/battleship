package core

object Config {
    const val DEFAULT_HOST = "localhost"
    const val DEFAULT_SERVER_PORT = 6789
    const val DEFAULT_LOBBY_SIZE = 2
}