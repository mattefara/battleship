package core

object Logger {
    fun <T> log(clazz: Class<T>, message: String) {
        println("${clazz.simpleName}:\t$message")
    }
}