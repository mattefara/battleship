package core.handlers

import core.Logger
import java.net.Socket

abstract class AsyncHandler(
    socket: Socket
) : Handler(socket), Runnable {

    abstract fun onMessageReceived(message: String)

    open fun onError(ex: Throwable) {
        Logger.log(this::class.java, "Client disconnected")
        socket.close()
    }

    open fun isListening(): Boolean {
        return !socket.isClosed
    }

    override fun run() {
        while (isListening()) {
            runCatching {
                val data = readMessage()
                onMessageReceived(data)
            }.apply {
                if (isFailure) {
                    onError(exceptionOrNull()!!)
                }
            }
        }
    }
}