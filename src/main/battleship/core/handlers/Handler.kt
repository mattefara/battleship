package core.handlers

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.DataInputStream
import java.io.DataOutputStream
import java.net.Socket

open class Handler(
    internal val socket: Socket
) {

    private val inputStream = DataInputStream(socket.getInputStream())
    private val outputStream = DataOutputStream(socket.getOutputStream())

    fun readMessage(): String {
        return inputStream.readUTF()
    }

    fun writeMessage(message: String) {
        outputStream.writeUTF(message)
        outputStream.flush()
    }

    internal inline fun <reified T> writeMessage(message: T) {
        writeMessage(Json.encodeToString(message))
    }

    internal inline fun <reified T> readMessage(): T {
        return Json.decodeFromString(readMessage())
    }

    fun close() {
        socket.close()
    }

}