package core.data

import kotlinx.serialization.Serializable

@Serializable
data class ConnectionPayload(
    val address: String,
    val port: Int,
    var start: Boolean = false
)
