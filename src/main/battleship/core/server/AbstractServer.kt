package core.server

import core.Config
import java.net.ServerSocket
import java.net.Socket

abstract class AbstractServer(
    port: Int = Config.DEFAULT_SERVER_PORT
) {

    internal val serverSocket = ServerSocket(port)

    abstract fun isListening(): Boolean

    abstract fun onStartedListening()
    abstract fun onAcceptedConnection(connection: Socket)
    abstract fun onStoppedListening()
    abstract fun onError(exception: Throwable)

    fun listen() {
        onStartedListening()
        while (isListening()) {
            val result = runCatching {
                val connection = serverSocket.accept()
                onAcceptedConnection(connection)
            }
            if (result.isFailure) {
                onError(result.exceptionOrNull()!!)
            }
        }
        onStoppedListening()
    }

    open fun close() {
        serverSocket.close()
    }

}