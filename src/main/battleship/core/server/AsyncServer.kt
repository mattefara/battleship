package core.server

import core.Config
import core.Logger
import java.net.Socket

class AsyncServer(
    port: Int = Config.DEFAULT_SERVER_PORT
) : AbstractServer(port), Runnable {

    lateinit var connection: Socket

    override fun isListening(): Boolean {
        return !serverSocket.isClosed
    }

    override fun onStartedListening() {
        Logger.log(this::class.java, "Async server started on port ${serverSocket.localPort}")
    }

    override fun onAcceptedConnection(connection: Socket) {
        this.connection = connection
        serverSocket.close()
    }

    override fun onStoppedListening() {
        Logger.log(this::class.java, "Async server stopped on port ${serverSocket.localPort}")
    }

    override fun onError(exception: Throwable) {

    }

    override fun run() {
        listen()
    }

}