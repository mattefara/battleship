package server

fun main() {
    LobbyServer().apply {
        listen()
        close()
    }
}