package server.lobby

import core.Config
import core.Logger
import server.lobby.handlers.LobbyHandler
import java.net.Socket


class Lobby(
    private val capacity: Int = Config.DEFAULT_LOBBY_SIZE
) {

    private val activeConnections = arrayListOf<Socket>()

    fun isFull(): Boolean {
        return activeConnections.size == capacity
    }

    fun addConnection(socket: Socket): Boolean {
        if (isFull()) {
            return false
        }
        activeConnections.add(socket)
        return true
    }

    fun startInBackground() {
        val handler = LobbyHandler(activeConnections)
        handler.readMessages()
        handler.broadcast()
    }

    fun close() {
        Logger.log(this::class.java, "Closing connections")
        for (connection in activeConnections) {
            if (!connection.isClosed) {
                connection.close()
            }
        }
    }

}