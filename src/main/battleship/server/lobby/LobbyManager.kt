package server.lobby


import core.Logger
import java.net.Socket

class LobbyManager {

    private val lobbies = arrayListOf<Lobby>()
    private var currentLobby = Lobby()

    fun add(connection: Socket) {
        Logger.log(this::class.java, "Attempt to add a player to existing lobby")
        if (connection.isClosed) {
            Logger.log(this::class.java, "Cannot add a closed connection")
        }
        val success = currentLobby.addConnection(connection)
        if (!success) {
            Logger.log(this::class.java, "Cannot add player to lobby, creating a new one (${lobbies.size})...")
            lobbies.add(currentLobby)
            currentLobby = Lobby()
            currentLobby.addConnection(connection)
        }
        if (currentLobby.isFull()) {
            Logger.log(this::class.java, "Lobby is now full, spawning a thread")
            currentLobby.startInBackground()
        }
        Logger.log(this::class.java, "Waiting for other players")
    }

    fun close() {
        for (lobby in lobbies) {
            lobby.close()
        }
    }

}