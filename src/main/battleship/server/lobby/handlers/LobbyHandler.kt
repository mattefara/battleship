package server.lobby.handlers

import core.Logger
import core.data.ConnectionPayload
import core.handlers.AsyncHandler
import core.handlers.Handler
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.net.Socket
import java.util.concurrent.ConcurrentHashMap
import kotlin.random.Random
import kotlin.random.nextInt


class LobbyHandler(
    private val connections: ArrayList<Socket>,
) {

    private val handlers = setupHandlers()
    private val bufferedMessages = ConcurrentHashMap<Socket, ConnectionPayload>()
    private val startingPlayer = pickStartingPlayer()

    private fun pickStartingPlayer(): Int {
        return Random.nextInt(0 until connections.size)
    }

    private fun setupHandlers(): List<AsyncHandler> {
        return connections.map { socket ->
            object : AsyncHandler(socket) {

                private var stop = false

                override fun isListening(): Boolean {
                    return !stop && super.isListening()
                }

                override fun onMessageReceived(message: String) {
                    val payload: ConnectionPayload = Json.decodeFromString(message)
                    bufferedMessages[socket] = payload
                    stop = true
                }
            }
        }
    }

    fun readMessages() {
        val threads = handlers.map {
            Thread(it).apply {
                start()
            }
        }
        for (thread in threads) {
            thread.join()
        }
        Logger.log(this::class.java, "Shutting down lobby handler")
    }

    fun broadcast(): Boolean {
        bufferedMessages.onEachIndexed { i, entry ->
            bufferedMessages.filterKeys { socket -> socket.hashCode() != entry.key.hashCode() }
                .values.forEach { payload ->
                    if (i == startingPlayer) {
                        payload.start = true
                    }
                    val dataHandler = Handler(entry.key)
                    dataHandler.writeMessage(Json.encodeToString(payload))
                    dataHandler.close()
                }
        }
        return true
    }


}