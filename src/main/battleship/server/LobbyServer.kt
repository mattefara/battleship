package server

import core.Config
import core.Logger
import core.server.AbstractServer
import server.lobby.LobbyManager
import java.net.Socket

class LobbyServer(
    port: Int = Config.DEFAULT_SERVER_PORT
) : AbstractServer(port) {

    private val lobbyManager = LobbyManager()

    override fun isListening(): Boolean {
        return !serverSocket.isClosed
    }

    override fun onStartedListening() {
        Logger.log(this::class.java, "Server started on port ${serverSocket.localPort}...")
    }

    override fun onAcceptedConnection(connection: Socket) {
        Logger.log(this::class.java, "Accepted connection from a player")
        lobbyManager.add(connection)
    }

    override fun onStoppedListening() {
        Logger.log(this::class.java, "Server stopped")
    }

    override fun onError(exception: Throwable) {

    }

    override fun close() {
        lobbyManager.close()
    }
}